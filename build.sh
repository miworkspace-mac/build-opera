#!/bin/bash -ex

# CONFIG
prefix="Opera"
suffix=""
munki_package_name="Opera"
display_name="Opera"
category="Productivity"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.tar.xz -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36' "${url}"

# mountpoint=`echo y | hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
tar -xf app.tar.xz

mkdir -p build-root/Applications
stapp=`ls -d *.app`
# Obtain version info
#version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" ${stapp}/Contents/Info.plist`
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" ${stapp}/Contents/Info.plist`

mv "${stapp}" build-root/Applications

# Obtain version info
#cp "${mountpoint}/Opera.app/Contents/Info.plist" Info.plist
#version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" ${mountpoint}/Opera.app/Contents/Info.plist`
#cp -R "${mountpoint}/Opera.app" build-root/Applications

#hdiutil detach "${mountpoint}"

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg --postinstall_script="post_install.sh" ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" category -string "${category}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

