#!/bin/bash

# LOC=`curl -L "http://get.geo.opera.com/ftp/pub/opera/desktop/" 2>/dev/null | grep [0-9] | tail -1 | awk -F'"' '{ print $2 }' | tr -d '\r'`

# NEWLOC=`curl -L "http://get.geo.opera.com/ftp/pub/opera/desktop/${LOC}mac/" 2>/dev/null | grep '.*\.dmg' | awk -F'"' '{ print $2 }' | head -1 | tr -d '\r'`

# if [ "x${NEWLOC}" != "x" ]; then
# 	echo "http://get.geo.opera.com/ftp/pub/opera/desktop/${LOC}mac/${NEWLOC}"
# fi


NEWLOC=`curl -L https://autoupdate.geo.opera.com/netinstaller/Stable/MacOS 2>/dev/null | grep [0-9] | tail -1 | awk -F'"' '{ print $4 }' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi