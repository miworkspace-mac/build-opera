#!/usr/bin/python
import plistlib
from LaunchServices import LSRegisterURL
from CoreData import CFURLRef
from sys import exit

app_path = '/Applications/Opera.app'
info_plist = '%s/Contents/Info.plist' % app_path
app_cfurl = CFURLRef.URLWithString_('file://%s' % app_path)

plist = plistlib.readPlist(info_plist)
plist['LSEnvironment'] =  {'OPERA_AUTOUPDATE_DISABLED': 'True'}
plistlib.writePlist(plist, info_plist)

LSRegisterURL(app_cfurl, True)

exit(0)
